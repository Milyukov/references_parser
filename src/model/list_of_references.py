class Reference(object):

    def __init__(self, line):
        self.original_line = line

    def parse_line(self):
        pass

    def format(self, template_format):
        pass

class ListOfReferences(object):

    def __init__(self, lines):
        self.references = []
        for line in lines:
            self.references.append(Reference(line))
            self.references[-1].parse_line()

    def format(self, template_format):
        for ref_index in range(len(self.references)):
            self.references[ref_index].format(template_format)

    def sort(self, sort_rule, article=None):
        pass
