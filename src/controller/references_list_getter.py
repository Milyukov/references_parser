class TextGetter(object):

    def __init__(self, path_to_text):
        self.path_to_text = path_to_text

    def get_text(self):
        pass

class TextFromTxt(TextGetter):

    def get_text(self):
        with open(self.path_to_text, 'r') as f:
            references = f.readlines()
        return references
