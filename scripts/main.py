from src.model.list_of_references import ListOfReferences
from src.controller.references_list_getter import TextFromTxt

import sys

if __name__ == '__main__':
    args = sys.argv
    script_path, path_to_text = args
    ref_text = TextFromTxt(path_to_text).get_text()
    list_of_references = ListOfReferences(ref_text)
