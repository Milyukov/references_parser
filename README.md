Project is dedicated to automatic changes in *.doc document after changes in references.

The following scenario is to be implemented:

| Must-haves |Performance benefits |Delighters |
| ---------- |-------------------- |---------- |
| Formatting of raw references list: parsing reference structure and type; formatting according to template | Sorting of list of references | Finding and loading templates for existing journals |
| Mapping references in text to table of references and formatting them according to template | Highlighting changes and detections in text | |
